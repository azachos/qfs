
Waterbase - Emissions
*********************

**Reporting obligation**

Data on emissions are collected through the WISE-SoE Reporting process (
`Water emission quality WISE 1
<http://www.eea.europa.eu/data-and-maps/indicators/oxygen-consuming-substances-in-rivers/oxygen-consuming-substances-in-rivers-5>`_).)
Following the test data request in 2008, data on emissions to water are requested as regular data flow from 2009 onwards.

The data requested on emissions include emissions of nutrients, organic matter and hazardous substances from a range of
both point and diffuse sources, as shown below.

.. image:: /common/images/emissions.jpg
   :width: 50%
   
There are different reporting processes for emission data:

*	Eurostat reporting on Inland waters (Joint Questionnaire OECD Eurostat Link) - if countries reported data under emissions
	to water in e.g. 2013, this information will be used to pre-fill the 2014 Eurostat/OECD Joint Questionnaire and no Eurostat
	reporting has to be done by the country;

*	Under the UWWT directive Link and for E-PRTR reporting Link;

*	Under the WFD (WFD River Basin Management Plan including programme of measures Link).
	The new WFD reporting guidance 2016 Link for the 2nd reporting includes concrete references to SoE reporting.
	WFD requirements on Inputs of pollutants to surface waters (and groundwaters), including inventories of emissions,
	discharges and losses of EQSD Annex I substances can be provided by reporting of SoE emissions.
*	Any data already submitted under SoE Quantity or SoE TCM waters (Riverine Load or Direct Discharges)
	reporting need not be resupplied under this reporting obligation.


(S10) Emissions
===============

**Use of data by the EEA**

Almost all human activities can and do impact adversely upon the water. Water quality is influenced by both direct point sources and
diffuse pollution, which comes from urban and rural populations, industrial emissions and farming. Member States are required to identify
significant point and diffuse sources of pollution in the River Basin District and the WFD requires that Member States collect and maintain
information on the type and magnitude of significant pressures. Member States are also obliged to prepare inventories for emissions,
discharges and losses for Priority and Hazardous Priority Substances.
The information reported will be used to formulate indicators of emissions that will be used in the assessment of pressures and states of
Europo s waters. These indicators will identify trends and help to evaluate the effectiveness of European policy and legislation.
During the preparation of new guidance for next WFD reporting, it has been incorporated that a source apportionment as reported with the
data dictionary of SoE emissions can be used for such purpose. In addition, improved assessment of the marine environment should result
from the quantification of emissions of land based pollutants (also in support of the work done by marine conventions).
The current EEA water resource efficiency indicators on emission intensities at country level on agriculture (WRE001),
domestic sector (WREI002) and manufacturing industry (WREI003) could potentially build on data from SoE emissions, provided a better
data coverage than until now.

Newly developed indicators based on the requested data could include:

*	Time series of annual emissions and any derived indices aggregated at a RBD/RB scale for each individual source
	(e.g. urban, industry, agriculture). Inter-annual hydrological variation will need to be accounted for when interpreting emissions
	time series.

*	Apportionment of emissions by source and by determinand within each national RBD/RB, showing where there are statistically
	significant increases, decreases and no changes over time. Such source apportionment identifies the key sectors to be targeted by measures


**EEA products**

*	Waterbase - Emissions to water Link

*	Water efficiency indicators
	*	Emission intensity of manufacturing industries in Europe (WREI 003) Link
	*	Emission intensity of domestic sector in Europe (WREI 002) Link
	*	Emission intensity of agriculture in Europe (WREI 001) Link
	*	Urban waste water treatment (CSI 024) Link
	*	(2004) Emissions to water of hazardous substances from urban sources Link
	*	(2004) Emissions to water of hazardous substances from industry Link

*	EEA reports
	*	EEA2005: Source apportionment of nitrogen and phosphorus inputs into the aquatic environment Link
	*	EEA 2010: Freshwater quality - SOER 2010 thematic assessment Link
	*	EEA 2011: Hazardous substances in Europe's fresh and marine waters - An overview Link

*	European Pollutant Release and Transfer Register - E-PRTR Link

**Reported data**

In the following tables there is an overview on reporting nutrients and hazardous substances from point sources and
diffuse sources and the groups of emission sources which have been used.

.. include:: /S10/text/ZZ.txt
   :start-after: start-text1-placeholder
   :end-before: end-text1-placeholder

  
.. figure:: /S10/tables/ZZ_table1.png
   :width: 100%
   
   Nutrients emissions from point sources and diffuse sources (with sources of emissions reported)
   
.. figure:: /S10/tables/ZZ_table2.png
   :width: 100%   

   Hazardous substances emissions from point sources and diffuse sources sources (with sources of emissions reported)

*Note*: The tables should give an overview if a country reports data on emissions from point and diffuse sources or if the
tables are prefilled from E-PRTR reporting, which determinands and which type of source apportionment have been reported.
In all tables the value means the number of spatial units in which the determinand was reported for that year.

*Note*: One aspect of the quality fact sheets is to improve the spatial coverage and ensure that stations are reported for all RBDs.

*Questions regarding the reporting on general water quality in groundwater SILVI ??*
------------------------------------------------------------------------------------

.. include:: /S10/questions/ZZ.txt




