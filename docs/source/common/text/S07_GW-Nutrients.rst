
Waterbase - Groundwater
***********************

Data on groundwater bodies are collected through the WISE-SoE data collection process (Groundwater quality EWN-3; Link).
Reporting under this obligation is used for EEA Core set of indicators (see EEA products).

The data requested on groundwater include the physical characteristics of the groundwater bodies,
proxy pressures on the groundwater area, location and parameters of groundwater monitoring stations including their relation
to groundwater body as well as chemical quality data on nutrients and organic matter, and hazardous substances in groundwater.
Determinands which should be reported under this data flow are shown in the following figure.

.. image:: /common/images/determinands_gw.jpg
   :width: 50%
   
Closely related to the SoE reporting on groundwater water quality is the reporting under the WFD
(WFD River Basin Management Plan including programme of measures Link).
The new WFD reporting guidance 2016 Link for the 2nd reporting includes concrete references to SoE reporting.
The information reported by EEA countries can only be interpreted reasonably when streamlined with the WFD reporting.
Combining water quality results with information on chemical status and potential, as foreseen in the WFD reporting guidance,
are only possible if the WFD water body is linked with SoE stations which provide long time series on relevant determinants.

(S07) Groundwater - Nutrients, Organic Matter and General Physico-Chemical Determinands
=======================================================================================

**Use of data by the EEA**

The information will be used to formulate indicators that will be used to assess state and trend of the determinands and monitor progress with
European policy objectives.
In addition, the information will be used to develop a European picture on water quality in a comparable way and to identify
potential problem areas at the European level. Assessments are also made periodically on the impact of particular socio-economic
sectors on water (e.g. the impact of agriculture on water), of particular issues (e.g. Groundwater quality and quantity in Europe Link).
Such assessments will be improved by the reporting of more detailed and less aggregated data. In EIONET-Water both detailed and aggregated
data are already reported. The focus should be shifted to disaggregated data.

**EEA products**

* Waterbase - Groundwater Link

* EEA indicators
	*	Nutrients in Freshwater (CSI 020) Link

* EEA reports
	*	EEA 2010: Freshwater quality - SOER 2010 thematic assessment. Link
	*	EEA ETC/ICM 2010: Freshwater Eutrophication Assessment - Background Report for EEA European Environment State and Outlook Report 2010. Link
	*	Water resources in Europe in the context of vulnerability Link
	*	EEA, 2012 European waters - assessment of status and pressures, EEA Report No 8/2012 Link
	*	EEA ETC/ICM 2012: Ecological and chemical status and pressures. Thematic assessment for EEA Water 2012 Link

* EEA maps
	*	Nitrates in groundwater by countries and WFD gw bodies Link
	*	Nitrites in groundwater by countries and WFD gw bodies Link
	*	Ammonium in groundwater by countries and WFD gw bodies Link Overview of
	*	SoE monitoring stations Link

* EEA datasets
	*	GIS layer on WFD groundwater bodies Link





**Reported data**

Table 4 1 and Table 4 2  provide an overview by determinands (highest priority nutrients) of the number of groundwater stations /
groundwater bodies per year.

.. S07

.. include:: /S07/text/ZZ.txt
   :start-after: start-text1-placeholder
   :end-before: end-text1-placeholder

  
.. figure:: /S07/tables/ZZ_table1.png
   :width: 100%
   
   Number of stations in which given determinand was reported per year (nutrients/organic pollutants in groundwater disaggregated)
   
.. figure:: /S07/tables/ZZ_table2.png
   :width: 100%   

   Number of groundwater bodies in which given determinand was reported per year (nutrients/organic pollutants in groundwater aggregated)


*Note*: In the current data set the reporting of high priority determinands has stopped or nutrients have been reported as aggregated data only.
The aim is to ensure that the high priority determinands (preferably disaggregated data) have as complete coverage as possible.


Table 4 3 shows the number of groundwater stations by River Basin Districts which reported on highest priority nutrients.


.. include:: /S07/text/ZZ.txt
   :start-after: start-text2-placeholder
   :end-before: end-text2-placeholder

  
.. figure:: /S07/tables/ZZ_table3.png
   :width: 100%
   
   Number of groundwater stations for nutrients/organic pollutants (disaggregated) by River Basin Districts
   
*Note*: One aspect of the quality fact sheets is to improve the spatial coverage and ensure that stations are reported for all RBDs.

*Questions regarding the reporting on general water quality in groundwater MIROSLAV / VIT ??*
---------------------------------------------------------------------------------------------

.. include:: /S07/questions/ZZ.txt



.. footnotes-placeholder

.. [#S07f1]    The lists of "preferred" substances are based on legislation, spatial and temporal availability.
      Preferred substances are also covered by Hazardous substances report regularly.         
         