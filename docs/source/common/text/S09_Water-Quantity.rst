Waterbase - Quantity
********************

**Reporting obligation**

Data on freshwater resources availability, abstraction and use at regional spatial scale are collected through the WISE-SoE annual data flow
(State and quantity of water resources EWN-4; Link). These data are primarily used to formulate indicators
(associated with the EEA's Core Set Indicators) and assess the state and trends of the water resources and associated pressures,
and monitor the progress with European policy objectives.

The information needed in relation to water quantity is generally described as drivers, pressures, state, impact, response.
In general there is a need for coherent European indicators describing water availability/scarcity in connection with water use and water use
efficiency to assess the extent and intensity of the problem, and of the impact of particular socio-economic sectors on water abstraction
(e.g. water abstraction by agriculture).

(S09) Water Quantity
====================

The main blocks of data collected through WISE-SoE are the following:

*	**Point data:** Individual measurements of streamflow, groundwater level and reservoir inflow/outflow within the specific reporting unit
	(e.g. RBD).
*	**Water Availability:** Components of the Water Balance as spatially aggregated data on a specific reporting unit such as
	hydrometeorological parameters (e.g. area precipitation, potential/actual evapotranspiration etc.), storage (e.g. snowpack,
	changes in reservoir and groundwater storage), returned flow, reused water, desalinated water, water imports/exports.
*	**Water Abstraction:** Total volume of freshwater abstraction from both surface water and groundwater for public water supply systems
	and for self-supply. Groundwater available for annual abstraction and evaporation losses (during transport and use).
*	**Water Use:** Total volume of freshwater used and breakdown by sector (according to NACE classes) provided by
	public water supply systems and self-supply as well as recycled water.

A detailed description of the requested data is given by the Water Quantity Data Manual Link

There are different reporting processes for water quantity data:

*	Eurostat reporting on Inland waters (Joint Questionnaire OECD Eurostat Link) - if countries reported data under emissions
	to water in e.g. 2013, this information will be used to pre-fill the 2014 Eurostat/OECD Joint Questionnaire and no Eurostat
	reporting has to be done by the country;
*	Under the WFD (WFD River Basin Management Plan including programme of measures Link).
	The new WFD reporting guidance 2016 Link for the 2nd reporting includes concrete references to SoE reporting.
	WFD requirements on "Water abstractions and exploitation of water resources" can be provided by reporting of SoE water quantity.

**Use of data by the EEA**

The information will be used to formulate indicators used to assess the state and trends of the water resources and associated pressures, and monitor the progress with European policy objectives. The information needed in relation to water quantity and quality can generally be described as:

*	**Drivers:** natural availability of water resources, hydro meteorological parameters, reservoirs management
*	**Pressures:** water demand, water abstraction, return flows by source and sector
*	**State:** assessment of trends by source
*	**Impacts:** ecosystem integrity, use value
*	**Responses:** Are policies working towards targets?  Such as are the rates of extraction from our water resources sustainable over
	the long term?

Water quantity data are also needed for the monitoring of water transfers, consumptions, abstractions and returns in properly defined spatial
and temporal scales across Europe. This is feasible through the ongoing development of water accounts, which is a system of water quantity
balance estimation between the natural environment and the various economic units (households, industry, etc.) that utilize water.
The development of this system is very important in order to confront the future challenges of increasing water use demand and shortage of
water availability due to climate change.

Another important issue for which water quantity data are needed is the increasing appearance of drought and water scarcity periods
especially in southern European Countries. In order to examine this phenomenon in a more consistent way the indicator WEI+ has been developed.
This indicator calculates the water consumption against the renewable water resources, quantifying the sustainability of water use in country
or RBD scale.


**EEA products**

*	Waterbase - Water quantity Link

*	Indicators

	*	Use of freshwater resources (CSI 018) Link

*	EEA reports

	*	EEA 2013: Results and lessons from implementing the Water Assets Accounts in the EEA area Link
	*	EEA 2012: Water resources in Europe in the context of vulnerability Link
	*	EEA 2012: Towards efficient use of water resources in Europe Link
	*	EEA ETC/ICM 2012: Vulnerability to Water Scarcity and Drought in Europe - Thematic assessment for EEA Water 2012 Report Link
	*	EEA 2010; Water resources: quantity and flows - SOER 2010 thematic assessment Link
	*	EEA 2009: Water resources across Europe - confronting water scarcity and drought Link

*	EEA maps

	*	Water quantity - Location of Streamflow Gauging Stations Link

In addition, results on water quantity are included into EEA products on climate change impacts, hazardous (drought), and sectors use of
water resource (e.g. households water use, irrigation water use, cooling water, hydropower production etc.).


.. include:: /S09/text/ZZ.txt
   :start-after: start-text1-placeholder
   :end-before: end-text1-placeholder

  
.. figure:: /S09/tables/ZZ_table3.png
   :width: 100%
   
   xxxx no title not defined

*Note:* Both point and spatial data are requested in the WISE-SoE water quantity dataflow.
Member states are requested to report in the smallest available temporal and spatial scales in order to identify
issues concerning water shortage or overconsumption. For example: If water abstraction is a significant pressure for a specific RBD in a
country with adequate water resources and data are reported only at country level, this issue will probably not be identified.

*Questions regarding the reporting on water quantity WHO??*
-----------------------------------------------------------

.. include:: /S09/questions/ZZ.txt

