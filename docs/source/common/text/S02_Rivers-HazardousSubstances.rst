
(S02)  Rivers - Hazardous Substances
====================================

**Use of data by EEA**

The information will be used to formulate indicators that will be used to assess state and trend of the determinand and monitor progress
with European policy objectives.
In addition, the information will be used to develop a European picture on water quality in a comparable way and to identify
potential problem areas at the European level. Assessments are also made periodically on the impact of particular socio-economic
sectors on water (e.g. the impact of agriculture on water), and European datasets made publicly available via Waterbase from the
European  water data centre Link can be used by research communities for assessments and perspectivation of own research
(e.g. relating to toxicity data  )). Development of a more user-friendly data viewer based on already developed maps and graphs
in the ETC/ICM technical report is foreseen as well as re-considering content of EEA indicator to display indexed state and trends.

**EEA products**

*	Waterbase - Rivers Link

*	EEA indicators

	*	Hazardous substances in rivers (WHS2) Link

*	EEA reports

	*	EEA Topic Report no. 2 / 2003. "Hazardous substances in the European marine environment - Trends in metals and persistent
		organic pollutants" (http://reports.eea.eu.int/topic_report_2003_2/en)
	*	EEA Technical Report  no. 8 / 2011. "Hazardous substances in Europes fresh and marine waters - an overview" Link
	*	EEA 2014: EEA ETC/ICM Technical report on hazardous substances Link
	*	ETC/ICM  Technical Report no. 1 / 2013. "Hazardous Substances in European waters -
		Analysis of the data on hazardous substances in groundwater, rivers, transitional, coastal and marine waters reported to the
		European Environment Agency from 1998 - 2010" Link  - update to be published 2014.


**Reported data**

Table :ref:`S02-tab1` provides an overview on the reporting of the preferred hazardous substances [#S02f1]_ sorted into various groups.
In addition to the substances being on the (WFD) list of priority substances, substances commonly monitored in several countries
are highlighted as well as other organic substances more seldom monitored (and not subject to same QA process for publication in Waterbase).
Furthermore an overview on the reporting of LOQ values is given.

.. include:: /S02/text/ZZ.txt
   :start-after: start-text1-placeholder
   :end-before: end-text1-placeholder

.. _S02-tab1:

.. figure:: /S02/tables/ZZ_table1.png
   :width: 100%
   
   Number of substances and stations for groups of preferred hazardous substances in rivers per year 


*Note:* In the current data set the reporting of some "preferred" determinands has stopped or there has been change in the
determinands in the database e.g. lead changed to dissolved lead. EEA wants to clarify if these changes are real changes or it has been
errors/misinterpretations introduced in compiling the databases. In addition, the aim is to ensure that the high priority determinands
(e.g. priority substances) have as complete coverage as possible.

Table :ref:`S02-tab2` shows the number of river stations by River Basin Districts which reported on "preferred" hazardous substances in which period.

.. include:: /S02/text/ZZ.txt
   :start-after: start-text2-placeholder
   :end-before: end-text2-placeholder

.. _S02-tab2:

.. figure:: /S02/tables/ZZ_table2.png
   :width: 100%
   
   Number of substances and stations for groups of preferred hazardous substances in rivers per year 


*Note:* One aspect of the quality fact sheets is to improve the spatial coverage and ensure that stations are reported for all RBDs.

*Questions regarding the reporting on hazardous substances in rivers: VIT*
--------------------------------------------------------------------------

.. include:: /S02/questions/ZZ.txt


.. footnotes-placeholder

.. [#S02f1]    The lists of "preferred" substances are based on legislation, spatial and temporal availability.
      Preferred substances are also covered by Hazardous substances report regularly.  