
(S08) Groundwater - Hazardous Substances
========================================

**Use of data by EEA**

The information will be used to formulate indicators that will be used to assess state and trend of the determinands and
monitor progress with European policy objectives. In addition, the information will be used to develop a European picture on
water quality in a comparable way and to identify potential problem areas at the European level. Assessments are also made
periodically on the impact of particular socio-economic sectors on water (e.g. the impact of agriculture on water), of
particular issues (e.g.
`Groundwater quality and quantity in Europe
<http://www.eea.europa.eu/data-and-maps/indicators/oxygen-consuming-substances-in-rivers/oxygen-consuming-substances-in-rivers-5>`_).
Such assessments will be improved by the reporting of detailed data.

**EEA products:**

*	`Waterbase Groundwater
	<http://www.eea.europa.eu/data-and-maps/indicators/oxygen-consuming-substances-in-rivers/oxygen-consuming-substances-in-rivers-5>`__

*	EEA indicators

	*	`Pesticides in Groundwater (WHS1a)
		<http://www.eea.europa.eu/data-and-maps/indicators/oxygen-consuming-substances-in-rivers/oxygen-consuming-substances-in-rivers-5>`__

*	EEA reports

	*	`EEA 2014: EEA ETC/ICM Technical report on hazardous substances
		<http://www.eea.europa.eu/data-and-maps/indicators/oxygen-consuming-substances-in-rivers/oxygen-consuming-substances-in-rivers-5>`__
	*	`EEA 2011: Hazardous substances in Europe's fresh and marine waters - An overview
		<http://www.eea.europa.eu/data-and-maps/indicators/oxygen-consuming-substances-in-rivers/oxygen-consuming-substances-in-rivers-5>`__

**Reported data**

Table 4 4 provides an overview on the reporting of the preferred hazardous substances [#S07f1]_ sorted into various groups.
In addition to the substances being on the (WFD) list of priority substances, substances commonly monitored in several countries are
highlighted as well as other organic substances more seldom monitored (and not subject to same QA process for publication in Waterbase).
Furthermore an overview on the reporting of LOQ values is given.



.. include:: /S08/text/ZZ.txt
   :start-after: start-text1-placeholder
   :end-before: end-text1-placeholder

  
.. figure:: /S08/tables/ZZ_table1.png
   :width: 100%
   
   Number of substances for groups of "preferred" hazardous substances in groundwater per year 

*Note*: In the current data set the reporting of some preferred determinands has
stopped or there has been change in the determinands in the database e.g. lead changed to dissolved lead.
EEA wants to clarify if these changes are real changes or it has been errors/misinterpretations introduced in compiling the databases.
In addition, the aim is to ensure that the high priority determinands (e.g. priority substances) have as complete coverage as possible.

Table 4.5 shows the number of groundwater stations by River Basin Districts which reported on preferred hazardous substances in which period.

.. include:: /S08/text/ZZ.txt
   :start-after: start-text2-placeholder
   :end-before: end-text2-placeholder


  
.. figure:: /S08/tables/ZZ_table2.png
   :width: 100%
   
   Number of groundwater stations by River Basin Districts and year for preferred hazardous substances
   
*Note*: One aspect of the quality fact sheets is to improve the spatial coverage and ensure that stations are reported for all RBDs.

*Questions regarding the reporting on hazardous substances in groundwater: VIT*
-------------------------------------------------------------------------------

.. include:: /S08/questions/ZZ.txt






