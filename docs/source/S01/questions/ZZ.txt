
**Clarifying questions on data in current database**

#. {x-determinand} has changed to {y-determinand} in year {YYYY}.
   Is that correct? Or would it be correct to change all {x-determinand} values to {y-determinand} ?

#. The data reported on {x-determinand} has gaps in the time-series.
   Does data exist to fill the gaps in the time-series?

#. The data reported on {x-determinand} in year(s) {YYYY,...} only includes some stations.
   Are there more stations where {x-determinand} was measured in these years?

#. No data was reported on {x-determinand} in year(s) {YYYY,...}.
   Was {x-determinand} measured in these years?

#. No data was reported on {x-determinand}.
   Are there stations where {x-determinand} is or was measured?

#. If {x-determinand} is not measured,
   what is the reason for not including it in the monitoring programmes?

   
**Improving coverage of determinands, temporal and spatial coverage**

#. Can data for years {YYYY,...} be redelivered?

#. Can data on {x-determinand} be redelivered?

#. Can complete time-series data on {x-determinand} be redelivered, to improve the temporal coverage?

#. Can more stations measuring {x-determinand} be delivered, to improve the spatial coverage?


**Links/references**

Please provide further references to

#. National water quality reports.

#. National water quality indicators.

#. National water quality data sets or databases.

