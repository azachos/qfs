
Waterbase - Rivers
******************

**Reporting obligation**

Data on rivers are collected through the WISE-SoE data collection process (River quality EWN-1; Link).
Biological quality elements in rivers are integrated into reporting of water quality starting from 2012 reporting period.

The data requested on rivers includes the physical characteristics of the river monitoring stations,
proxy pressures on the upstream catchment areas, as well as chemical quality data on nutrients and organic matter,
and hazardous substances in rivers.

It also includes the biological data (primarily calculated as national Ecological Quality Ratios EQR),
as well as information on the national classification systems for each biological quality element and waterbody type.
Determinands which should be reported under this data flow are shown in the following figure.

.. image:: /common/images/determinands_rivers.jpg
   :width: 80%

Closely related to the SoE reporting on rivers water quality is the reporting under the WFD (WFD River Basin Management
Plan including programme of measures Link). The new WFD reporting guidance 2016 Link for the 2nd reporting includes concrete references to SoE
reporting. The information reported by EEA countries can only be interpreted reasonably when streamlined with the WFD reporting.
Combining water quality results with information on ecological status and potential, as foreseen in the WFD reporting guidance, are only possible
if the WFD water body is linked with SoE stations which provide long time series on relevant determinants.

(S01) Rivers - Nutrients, Organic Matter and General Physico-Chemical Determinands
==================================================================================


**Use of data by EEA**

The information will be used to formulate indicators to assess the state and trend of
the determinand and monitor progress with European policy objectives.
In addition, the information will be used develop a European picture on water quality
in a comparable way and to identify potential problem areas at the European level.

Assessments are also made periodically on the impact of particular socio-economic sectors
on water (e.g. the impact of agriculture on water), of particular issues
(e.g. Nutrients in European ecosystems), and assessments based on methodologies
for weighted quality indices. Such assessments will be improved by the reporting of
more detailed and less aggregated data than currently collected by Eionet-Water.

**EEA products**

* Waterbase - Rivers
* EEA indicators
	*  	`Oxygen consuming substances in rivers (CSI 019)
	   	<http://www.eea.europa.eu/data-and-maps/indicators/oxygen-consuming-substances-in-rivers/oxygen-consuming-substances-in-rivers-5>`__
	*  	`Nutrients in Freshwater (CSI 020)
	  	<http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-3>`__
* EEA reports
	*	`EEA 2010: Freshwater quality - SOER 2010 thematic assessment.
	 	<http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-3>`__
	* 	`EEA ETC/ICM 2010: Freshwater Eutrophication Assessment - Background Report for EEA European Environment State and Outlook Report 2010.
		<http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-3>`__
	* 	`EEA, 2012 European waters - assessment of status and pressures, EEA Report No 8/2012
		<http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-3>`__
	* 	`EEA ETC/ICM 2012: Ecological and chemical status and pressures. Thematic assessment for EEA Water 2012 Link
		<http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-3>`__
* EEA maps
	* 	`WISE SoE Ammonium in Rivers
		<http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-3>`__
	* 	`WISE SoE: Orthophosphate in Rivers
		<http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-3>`__
	* 	`WISE SoE: BOD in Rivers
		<http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-3>`__
	* 	`Nitrate in Rivers
		<http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-3>`__
	* 	`Water quality in rivers and lakes
		<http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-3>`__



**Reported data**

Determinands
------------

Table 2 1 provides an overview of the determinands reported per year for the period 1992 to 2012.



..
	--------------------
	Insert the TEXT here
	---------------------
	
.. include:: /S01/text/ZZ.txt
   :start-after: start-text1-placeholder
   :end-before: end-text1-placeholder	


.. figure:: /S01/tables/ZZ_table1.png
   :width: 100%
   
   Number of stations with the determinands reported per year.	

*Note*: In the current data set the reporting of some high priority nutrients  has stopped or there has been a change in the determinands
in the database. EEA wants to clarify if these changes are real changes or it has been errors/misinterpretations introduced in compiling
the databases. In addition, the aim is to ensure that the high priority determinands (e.g. nitrate or orthophosphate) have as complete
coverage as possible.

Station coverage
----------------
.. include:: /S01/text/ZZ.txt
   :start-after: start-text2-placeholder
   :end-before: end-text2-placeholder

Table 2.2 shows the number of river stations by River Basin Districts which reported on nutrients for the period from 1992 - 2012.

.. figure:: /S01/tables/ZZ_table1.png
   :width: 88%
   
   Number of river stations by River Basin District/year.
   
*Note:* One aspect of the quality fact sheets is to improve the spatial coverage and ensure that stations are reported for all RBDs.

Time series
-----------

The trend evaluation in EEA water quality indicators (CSI19 and CSI20) is based on stations with data from the period 1992-2012 or 2000/2003-2012.

Note: EEA water quality indicators are used for trend assessments based on consistent time series with some gap filling.
For a single country consistent time series are established for the defined period (e.g. 1992-2012; or 2000-2012) with some gap filling
(e.g. up to 3 years)  and only stations with values for all years in the defined period are used.
This ensures that any trend is because of change in the observations and not in the stations included.
Sometimes there is a stop in reporting stations in some years; or stations cannot be clearly identified,
e.g. because of missing or changing names of stations.  EEA wants to clarify these issues and improve the time series for high priority
nutrients.

.. include:: /S01/text/ZZ.txt
   :start-after: start-text3-placeholder
   :end-before: end-text3-placeholder

*Questions regarding the reporting on general water quality in rivers: KARI / LIDJA*
------------------------------------------------------------------------------------

.. include:: /S01/questions/ZZ.txt
