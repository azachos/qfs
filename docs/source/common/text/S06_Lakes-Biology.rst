
(S06) Lakes - Biology
=====================

**Use of data by the EEA**

The information will be used to assess the ecological status in a comparable way across Europe and to identify potential problem areas at
the European level. Assessments are also made periodically on the impact of particular socio-economic sectors on water
(e.g. the impact of agriculture on water), and of particular issues (e.g. nutrients in European ecosystems). Such assessments will be improved by the reporting of data on the biological elements of water bodies. The national EQR values will be translated to normalised EQR values by EEA, based on the national EQR class boundaries and a simple interpolation technique, to allow the data to become comparable between countries and across regions.

The normalised data could be used to calculate a new EEA indicator, including for example:

*	Summaries of the normalised EQR values grouped into different status classes at different spatial scales: national,
	river basin district (RBD) or river basins (RBs).

*	Time series of the normalised EQR values for stations or water bodies aggregated for different European regions
	(e.g. GIG-regions) and different lake types (e.g. small lowland calcareous lakes).

*	Proportion of water bodies or stations within each country or within each RBD for which there are statistically
	significant increases, decreases and no changes in normalised EQR values over time.

*	Comparison of lake nutrients with lake biology for stations reported for both data flows. Correlations between Total P
	and phytoplankton normalised EQR, between Total P and chlorophyll, between Total P and cyanobacteria  and between Total P and
	macrophytes EQR or Total P and macrophytes growing depth (pressure-impact information)


**EEA products**

* `Waterbase - lakes
  <http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-3>`__
* EEA reports
	*	`EEA ETC/ICM 2012: Ecological and chemical status and pressures. Thematic assessment for EEA Water 2012
		<http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-3>`__
* EEA maps
	*	`Biological elements in rivers and lakes
		<http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-3>`__
	*	`Phytoplankton in lakes
		<http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-3>`__
	*	`Macrophytes in lakes
		<http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-3>`__


**Reported data**

Table 2 5 and Table 2 6 provide an overview of biology records per determinand, aggregation period and year resp.
per BQE, RBD and year for the country.


.. include:: /S03/text/ZZ.txt
   :start-after: start-text1-placeholder
   :end-before: end-text1-placeholder

  
.. figure:: /S06/tables/ZZ_table1.png
   :width: 100%
   
   Number of lake biology records per determinand, aggregation period and year.
   (a) EQR values and/or status class. (b) Additional metrics in original scale.
   
.. figure:: /S06/tables/ZZ_table2.png
   :width: 100%   

   Number of lake biology records per BQE, RBD and year. (a) EQR values and/or status class. (b) Additional metrics in original scale.
   
   
*Note*: Important questions in reporting on biology are if status classes and EQR values are reported from member countries and what
the reasons are if they are not reported. Relevant is also if reported EQR values can be normalised or if there are problems in doing so.
In general care should be taken to ensure that the data are consistent with the WFD reporting.
This means that the stations reported are geographically representative, as well as representative in terms of the WFD distribution of
ecological status classes, and that all major lake types are included.

*Questions regarding the reporting on biology in lakes: ANNE?*
--------------------------------------------------------------

.. include:: /S06/questions/ZZ.txt