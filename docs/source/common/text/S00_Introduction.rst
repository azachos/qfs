Introduction
************

The European Environment Agency (EEA)
manages water data and information
reported either voluntarily by EEA member countries
(water quality in groundwater, rivers, lakes; emissions of pollutants and water quantity);
and data reported via REPORTNET under EU water directives:
Water Framework Directive (WFD),
Bathing Water Directives (BWD),
Urban Waste Water Treatment Directive (UWWTD),
Nitrates Directive (NiD) and
Drinking Water Directive (DWD).

Reported data are processed at EEA and stored in water data centre.
They can be also accessible on EEA home page.
Data reported under Nitrates Directive (NiD) and Drinking Water Directive (DWD)
are not yet available at EEA water data centre home page.

The aim of this Data Quality Factsheet
is to support clean-up and error correction
in the data member countries now have reported for 15-20 years.
Some errors have been introduced by the EEA and its Topic Centres
handling of the reported data;
others are due to errors introduced in member countries reporting.

Another aspect is to improve the spatial and temporal coverage
and to ensure that the relevant determinands are reported.

*   In some cases member countries will be asked for more stations
    to increase the spatial coverage or density of stations;
    or questions on why data have not been reported from some of the RBDs.

*   EEA water quality indicators are for trend assessments
    based on consistent time series with some gap filling.
    For a single country, consistent time series are established for the defined period
    (e.g. 1992-2012; or 2000-2012) with some gap filling (e.g. up to 3 years)
    and only stations with values for all years in the defined period are used.
    This ensures that any trend is because of change in the observations and not in the stations included.
*   In the current data set
    the reporting of some high priority determinands has stopped
    or there has been change in the determinands in the database
    e.g. cadmium changed to dissolved cadmium.
    EEA wants to clarify if these changes are real changes
    or it has been errors/misinterpretations introduced in compiling the databases.
    In addition, the aim is to ensure that the high priority determinands
    (e.g. nitrate or orthophosphate) have as complete a coverage as possible.

The last part is on ensuring linkage between the different Waterbases
by having a common coding system (Water Body ID)
and linked to different reference layers such as the RBDcodes.
